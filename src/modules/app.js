import { merge } from "lodash";

import userReducer from "./user";

const initialState = {
    user: undefined
};

export default function reducer(state = initialState, action) {
    const user = merge({}, state.user, userReducer(state.user, action));

    return { ...state, user };
}

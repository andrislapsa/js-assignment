import { take, call, put, fork } from "redux-saga/effects";

import {
    sendMessage,
    RECEIVED_MESSAGE,
    CLOSED as WS_DISCONNECTED,
    CONNECTED as WS_CONNECTED
} from "./socket";

const initialState = {
    type: "",
    authorized: false,
    failure: false
};

const AUTHORISE = "user/AUTHORISE";
const LOGIN_SUCCEEDED = "user/LOGIN_SUCCEEDED";
const LOGIN_FAILED = "user/LOGIN_FAILED";

export function authorise(username, password) {
    return {
        type: AUTHORISE,
        payload: {
            $type: "login",
            username,
            password
        }
    };
}

function successfulAuthorisation(userType) {
    return {
        type: LOGIN_SUCCEEDED,
        payload: userType
    };
}

function failedAuthorisation() {
    return { type: LOGIN_FAILED };
}

window.authorise = authorise;

function* waitForAuthResponse() {
    let response;

    while (true) {
        response = yield take(RECEIVED_MESSAGE);

        if (
            response.payload.$type === "login_successful" ||
            response.payload.$type === "login_failed"
        ) {
            return response.payload;
        }
    }
}

export function* userSaga() {
    let authMessage;

    while (true) {
        if (!authMessage) {
            ({ payload: authMessage } = yield take(AUTHORISE));
        } else {
            // waiting for WS to be ready
            yield take(WS_CONNECTED);
            // afterwards - will attempt to re-auth with previous credentials
        }

        yield put(sendMessage(authMessage));

        const response = yield call(waitForAuthResponse);

        if (response.$type === "login_successful") {
            yield put(successfulAuthorisation(response.user_type));

            // only one successful auth response per socket will be returned
            yield take(WS_DISCONNECTED);
        } else {
            yield put(failedAuthorisation());
        }
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCEEDED :
            return {
                ...state,
                type: action.payload.type,
                authorized: true
            };

        case LOGIN_FAILED :
            return {
                ...state,
                failure: true,
                authorized: false
            };
    }

    return state;
}

import { take, call, put, fork } from "redux-saga/effects";

import { createSocket, createSource, url } from "../socketUtil";

export const CONNECTED = "socket/CONNECTED";
export const CLOSED = "socket/CLOSED";
export const RECEIVED_MESSAGE = "socket/RECEIVED_MESSAGE";

const DISCONNECT = "socket/DISCONNECT";
const SEND_MESSAGE = "socket/SEND_MESSAGE";
const PING = "socket/PING";


function receiveMessage(message) {
    return {
        type: RECEIVED_MESSAGE,
        payload: message
    };
}

function socketClosed() {
    return { type: CLOSED };
}

export function disconnect() {
    return { type: DISCONNECT };
}

export function connected() {
    return { type: CONNECTED };
}

export function sendMessage(message) {
    return {
        type: SEND_MESSAGE,
        payload: message
    };
}

function* watchMessages(source) {
    let message;

    while (true) {
        message = yield call(source.nextMessage);

        if (message) {
            yield put(receiveMessage(message));
        } else {
            // breaking the loop when message promise gets rejected
            yield put(socketClosed());
            return;
        }
    }
}

function* socketKiller(socket) {
    yield take(DISCONNECT);
    socket.close();
}

function* sendMessages(socket) {
    let action;

    while (true) {
        action = yield take(SEND_MESSAGE);

        if (socket.readyState !== 1) {
            // time for this saga to end
            // new one will be spawned in socketLifeCycle
            return;
        }

        socket.send(JSON.stringify(action.payload));
    }
}

export function* socketLifeCycle() {
    let socket, source;

    while (true) {
        // creating new socket instance
        socket = yield call(createSocket, url);

        // creating source on which we will observe received socket messages
        source = yield call(createSource, socket);

        // starting new message watcher saga
        yield fork(watchMessages, source);

        // starting new saga that will send WS messages on SEND_MESSAGE actions
        yield fork(sendMessages, socket);

        // creating saga that will wait for user action to close socket
        yield fork(socketKiller, socket);
        
        // dispatch letting know that WS ir ready
        yield put(connected());

        // waiting until socket is closed
        yield take(CLOSED);
    }
}

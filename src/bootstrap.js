import React from "react";
import { render } from "react-dom";
import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";

import Lobby from "./components/Lobby";
import reducer from "./modules/app";
import { socketLifeCycle } from "./modules/socket";
import { userSaga } from "./modules/user";

const store = createStore(reducer, undefined, compose(
    applyMiddleware(createSagaMiddleware(
        socketLifeCycle,
        userSaga
    )),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

window.store = store;

render(
    <Provider store={store}>
        <Lobby />
    </Provider>,
    document.querySelector("#root")
);

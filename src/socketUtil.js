export const url = "wss://js-assignment.evolutiongaming.com/ws_api";

export async function createSocket(url) {
    const socket = new WebSocket(url);
    
    const deferred = {};
    const connectedPromise = new Promise(
        (resolve, reject) => {
            deferred.resolve = resolve;
            deferred.reject = reject;
        }
    );
    
    socket.onopen = () => {
        deferred.resolve(socket);
    };

    return connectedPromise;
}

export function createSource(socket) {
    let deferred;

    socket.onmessage = event => {
        if (deferred) {
            deferred.resolve(JSON.parse(event.data));
            deferred = null;
        }
    };

    socket.onclose = () => {
        if (deferred) {
            deferred.reject();
        }
    };

    socket.onerror = () => {
        if (deferred) {
            deferred.reject();
        }
    };

    return {
        nextMessage() {
            if (!deferred) {
                deferred = {};
                deferred.promise = new Promise(
                    (resolve, reject) => {
                        deferred.resolve = resolve;
                        deferred.reject = reject;
                    }
                );
            }
            return deferred.promise;
        }
    };
}

import React from "react";
import { connect } from "react-redux";
import { partial } from "lodash";

import { authorise } from "../modules/user";


class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: ""
        };
        this.usernameChanged = partial(this.handleChanges, "username");
        this.passwordChanged = partial(this.handleChanges, "password");
    }

    authorize() {
        this.props.authorise(this.state.username, this.state.password);
    }

    handleChanges(field, e) {
        this.setState({
            [field]: e.target.value
        });
    }

    render() {
        if (this.props.authorized) {
            return (
                <div>Login successful!</div>
            );
        }

        return (
            <div>
                <input type="text" placeholder="Username" onChange={(e) => this.usernameChanged(e)} />
                
                <input type="password" placeholder="Password" onChange={(e) => this.passwordChanged(e)} />

                <button onClick={() => this.authorize()}>
                    Login
                </button>

                {
                    this.props.authorisationFailure &&
                    <div className="authorisation-failure">
                        Wrong username and/or password!
                    </div>
                }
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        authorise: (username, password) => {
            dispatch(authorise(username, password));
        }
    };
};

const mapStateToProps = (state) => {
    return {
        authorisationFailure: state.user.failure,
        authorized: state.user.authorized
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

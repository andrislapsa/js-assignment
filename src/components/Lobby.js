import React from "react";
import { connect } from "react-redux";

import { sendMessage, disconnect } from "../modules/socket";
import Login from "./Login";


class Lobby extends React.Component {
    render() {
        return (
            <div>
                <Login />

                <button onClick={this.props.disconnect}>disconnect</button>

                <button onClick={this.props.ping}>ping</button>
            </div>
        );
    }
}

let pingSeq = 0;

const mapDispatchToProps = (dispatch) => {
    return {
        ping: () => {
            dispatch(sendMessage({
                $type: "ping",
                seq: ++pingSeq
            }));
        },
        disconnect: () => {
            dispatch(disconnect());
        }
    };
};

const mapStateToProps = (state) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Lobby);
